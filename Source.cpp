#include <Windows.h>
#include <iostream>
#include <string>
#include "Helper.h"
using namespace std;
void main()
{
	string command;
	string path;
	string folder;
	string app;
	TCHAR s[2048];
	Helper h;
	while (true)
	{
		cout << "<<";

		getline(cin, command);
		if (command == "pwd")
		{
			GetCurrentDirectoryA(2048, s);
			cout << s << endl;
		}
		else if (h.get_words(command)[0] == "cd")
		{
			path = h.get_words(command)[1];
			SetCurrentDirectoryA(path.c_str());
		}
		else if (h.get_words(command)[0] == "create")
		{
			folder = h.get_words(command)[1];
			CreateDirectoryA(folder.c_str(), NULL);
		}
		else if (command == "ls")
		{
			WIN32_FIND_DATA data;
			GetCurrentDirectoryA(2048, s);
			path = string(s);
			path = path + "\\*";
			HANDLE hFind = FindFirstFile(path.c_str(), &data);      // DIRECTORY
			if (hFind != INVALID_HANDLE_VALUE) {
				do {
					std::cout << data.cFileName << std::endl;
				} while (FindNextFile(hFind, &data));
				FindClose(hFind);
			}
		}
		else if (command == "secret")
		{
			HINSTANCE dllHandle = LoadLibrary(TEXT("Secret.dll"));
			typedef int(*TheAnswerToLifeTheUniverseAndEverything)(void);
			FARPROC pTest = GetProcAddress(HMODULE(dllHandle), "TheAnswerToLifeTheUniverseAndEverything");

			TheAnswerToLifeTheUniverseAndEverything obj;
			obj = TheAnswerToLifeTheUniverseAndEverything(pTest);
			cout << obj() << endl;
			FreeLibrary(dllHandle);
		}
		else if (command.find("exe") != string::npos)
		{
			HANDLE h = ShellExecute(0, "open", command.c_str(), NULL, NULL, 1);
			DWORD exitCode;
			WaitForSingleObject(h, INFINITE);
			if (GetExitCodeProcess(h, &exitCode))
			{
				
				if (exitCode == STATUS_PENDING)
					cout << "Yea" << endl;
				else
				{
					cout << "Failed to run exe file" << endl;
					cout << "Code = " << exitCode << endl;
				}
					
			}
		}
	}
}